//
//  FuzzImage+CoreDataProperties.swift
//  FuzzTest
//
//  Created by Nicholas Galasso on 8/29/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension FuzzImage {

    @NSManaged var contentURL: NSURL

}
