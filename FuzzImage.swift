//
//  FuzzImage.swift
//  FuzzTest
//
//  Created by Nicholas Galasso on 8/29/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation
import CoreData


class FuzzImage: FuzzObject {

    class func image(contentURL:NSURL)->FuzzImage{
        let ctx = AppDelegate.sharedInstance().managedObjectContext
        let i = NSEntityDescription.insertNewObjectForEntityForName("FuzzImage", inManagedObjectContext: ctx) as! FuzzImage
        i.contentURL = contentURL
        return i
    }

}
