//
//  WebViewController.swift
//  FuzzTest
//
//  Created by Nicholas Galasso on 8/30/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation
import WebKit

class WebViewController : UIViewController {
    
    let url:NSURL
    
    init(url:NSURL){
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = WKWebView(frame: CGRectZero)
    }
    
    func webView()->WKWebView{
        return view as! WKWebView
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let req = NSURLRequest(URL: url)
        webView().loadRequest(req)
    }
}