//
//  Backend.swift
//  FuzzTest
//
//  Created by Nicholas Galasso on 8/24/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation
import CoreData

//MARK:- Network Request

func getData(completion:(Bool)->() ){
    
    guard let url = NSURL(string: "http://quizzes.fuzzstaging.com/quizzes/mobile/1/data.json") else {
        assertionFailure("invalid url")
        return
    }
    
    let req = NSMutableURLRequest(URL: url)
    req.HTTPMethod = "GET"
    
    let task = NSURLSession.sharedSession().dataTaskWithRequest(req) {
        
        (data:NSData?, resp:NSURLResponse?, err:NSError?) in
        
        if let r = resp as? NSHTTPURLResponse {
            
            var successful = false
            
            switch r.statusCode {
            case 200:
                successful = true
                break
            default:
                break
            }
            
            if let d = data where successful {
                do {
                    if let body = try NSJSONSerialization.JSONObjectWithData(d, options: []) as? NSArray {
                        parse(body)
                    }
                } catch {
                    print("exception while deserializing response body from \(resp)")
                }
                
            } else {
                print("request failed")
            }
            
            completion(successful)
        }
    }
    
    task.resume()
}

func emptyDatabase(){
    let deleteRequest = NSFetchRequest(entityName: "FuzzObject")
    
    do {
        if let oldObjs = try AppDelegate.sharedInstance().managedObjectContext.executeFetchRequest(deleteRequest) as? [NSManagedObject]{
            for o in oldObjs {
                AppDelegate.sharedInstance().managedObjectContext.deleteObject(o)
            }
        }
        AppDelegate.sharedInstance().saveContext()
        
    } catch {
        assertionFailure()
    }
}

//MARK:- Parsing

func parse(response:NSArray){
    
    emptyDatabase()
    
    response.enumerateObjectsUsingBlock { (object, _, _) in
        
        guard
            let obj = object as? NSDictionary,
            let typeStr = obj[.Type] as? String
            else {
                print("dropped unreadable object")
                return
        }
        
        switch typeStr {
        case "image":
            if let _ = obj.fuzzImage() {
                print("received image")
            }
        case "text":
            if let _ = obj.fuzzText() {
                print("received text")
            }
        default:
            print("dropped object with unknown type")
            break
        }
    }
    
    do {
        try AppDelegate.sharedInstance().managedObjectContext.save()
    } catch {
        assertionFailure()
    }
}

//MARK:- make parsing prettier

enum FuzzKeys:String {
    case Date = "date"
    case Ident = "id"
    case Type = "type"
    case Data = "data"
}

//share a formatter, they are expensive to init
let dateFormatter:NSDateFormatter = {
    let f = NSDateFormatter()
    f.dateFormat = "MM/dd/YYYY"
    return f
}()

protocol FuzzDeserialization{}
extension NSDictionary:FuzzDeserialization {}
extension FuzzDeserialization where Self:NSDictionary {
    
    //allows us to pass FuzzKeys as subscripts, ie. let dataStr = self[.Data] as? String
    subscript (key:FuzzKeys) -> AnyObject? {
        get {
            return self[key.rawValue]
        }
    }
    
    func fuzzImage() -> FuzzImage? {
        guard
            let dataStr = self[.Data] as? String,
            let dataURL = NSURL(string: dataStr),
            let dateStr = self[.Date] as? String,
            let date = dateFormatter.dateFromString(dateStr),
            let idStr = self[.Ident] as? NSString
            else {
                print("malformed image object")
                return nil
        }
        
        let img = FuzzImage.image(dataURL)
        img.date = date
        img.ident = NSNumber(integer: idStr.integerValue)
        return img
    }
    
    func fuzzText() -> FuzzText? {
        guard
            let dataStr = self[.Data] as? String,
            let dateStr = self[.Date] as? String,
            let date = dateFormatter.dateFromString(dateStr),
            let idStr = self[.Ident] as? NSString
            else {
                print("malformed text object")
                return nil
        }
        
        let txt = FuzzText.text(dataStr)
        txt.date = date
        txt.ident = NSNumber(integer: idStr.integerValue)
        return txt
    }
}