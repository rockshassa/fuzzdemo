//
//  ViewController.swift
//  FuzzTest
//
//  Created by Nicholas Galasso on 8/23/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    let tableView = UITableView()
    let control = UISegmentedControl(items:FuzzContentMode.items())
    let imageCache = NSCache()
    var firstView = true
    
    let sortDescriptor = [NSSortDescriptor(key: "date", ascending: false)]
    
    var frc:NSFetchedResultsController = {
    
        let req = NSFetchRequest()
        
        req.entity = NSEntityDescription.entityForName("FuzzObject", inManagedObjectContext: AppDelegate.sharedInstance().managedObjectContext)
        req.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        
        let controller = NSFetchedResultsController(fetchRequest: req, managedObjectContext: AppDelegate.sharedInstance().managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try controller.performFetch()
        } catch {
            assertionFailure()
        }
        
        return controller
    }()
    
    let reuseIdentifier = "reuse"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "FuzzDemo"
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        control.translatesAutoresizingMaskIntoConstraints = false
        control.selectedSegmentIndex = 0;
        control.addTarget(self, action: #selector(controlUpdated), forControlEvents: .ValueChanged)
        
        frc.delegate = self
        
        view.addSubview(tableView)
        
        let controlHeight:CGFloat = 80
        view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .Top, relatedBy: .Equal, toItem: view, attribute: .Top, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .Leading, relatedBy: .Equal, toItem: view, attribute: .Leading, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .Width, relatedBy: .Equal, toItem: view, attribute: .Width, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .Height, relatedBy: .Equal, toItem: view, attribute: .Height, multiplier: 1, constant: -controlHeight))
        
        view.addSubview(control)
        
        view.addConstraint(NSLayoutConstraint(item: control, attribute: .Top, relatedBy: .Equal, toItem: tableView, attribute: .Bottom, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: control, attribute: .Leading, relatedBy: .Equal, toItem: view, attribute: .Leading, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: control, attribute: .Width, relatedBy: .Equal, toItem: view, attribute: .Width, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: control, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: controlHeight))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if (firstView){
            firstView = false
            
            let getDataHandler = { [unowned self] (success:Bool) in
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                })
            }
            
            getData(getDataHandler)
        }
    }
    
    func controlUpdated(){
        
        let req = NSFetchRequest(entityName: control.fuzzContentMode.entityName())
        req.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        
        frc = NSFetchedResultsController(fetchRequest: req, managedObjectContext: AppDelegate.sharedInstance().managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try frc.performFetch()
        } catch {
            assertionFailure()
        }
        
        tableView.reloadData()
        tableView.scrollToRowAtIndexPath(NSIndexPath(forRow:0, inSection: 0), atScrollPosition: .Top, animated: true)
    }
}

extension ViewController :NSFetchedResultsControllerDelegate{
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = frc.fetchedObjects?.count{
            return count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath)
        
        let obj = self.frc.objectAtIndexPath(indexPath)
        
        if let imgObj = obj as? FuzzImage {
            if let image = imageCache.objectForKey(imgObj.contentURL) as? UIImage {
                cell.imageView?.image = image
                cell.textLabel?.text = imgObj.contentURL.absoluteString
            } else {
                cell.imageView?.image = nil
                cell.textLabel?.text = "image not available \(imgObj.contentURL.absoluteString)"
                getImage(imgObj, indexPath: indexPath)
            }
        } else if let textObj = obj as? FuzzText {
            cell.imageView?.image = nil
            cell.textLabel?.text = textObj.text
        }
        
        return cell
    }
    
    func getImage(obj:FuzzImage, indexPath:NSIndexPath) {
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(obj.contentURL) { [unowned self] (data, resp, error) in
            
            if let imageData = data {
                
                if let img = UIImage(data: imageData) {
                    self.imageCache.setObject(img, forKey: obj.contentURL)
                    
                    dispatch_async(dispatch_get_main_queue(), { 
                        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    })
                    
                } else {
                    print("failed to fetch image at \(obj.contentURL.absoluteString)")
                }
            }
        }
        task.resume()
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let objects = frc.fetchedObjects else {
            assertionFailure()
            return
        }
        
        let obj = objects[indexPath.row]
        
        if let img = obj as? FuzzImage {
            let vc = WebViewController(url: img.contentURL) //todo: pull from tableView's image cache
            self.navigationController?.pushViewController(vc, animated: true)
        } else if obj is FuzzText {
            let url = NSURL(string:"https://fuzzproductions.com/")
            let vc = WebViewController(url: url!)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- For convenience

enum FuzzContentMode : Int {
    case All = 0
    case Text = 1
    case Images = 2
    
    func stringValue()->String{
        switch self {
        case .All: return "All"
        case .Text: return "Text"
        case .Images: return "Image"
        }
    }
    
    static func items()->[String]{
        return [FuzzContentMode.All.stringValue(), FuzzContentMode.Text.stringValue(), FuzzContentMode.Images.stringValue()]
    }
    
    func entityName()->String{
        switch self {
        case .All:
            return "FuzzObject"
        case .Images:
            return "FuzzImage"
        case .Text:
            return "FuzzText"
        }
    }
}

extension UISegmentedControl {
    
    var fuzzContentMode:FuzzContentMode {
        switch self.selectedSegmentIndex {
        case FuzzContentMode.Text.rawValue:
            return .Text
        case FuzzContentMode.Images.rawValue:
            return .Images
        default:
            return .All
        }
    }
}

