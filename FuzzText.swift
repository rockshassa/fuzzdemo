//
//  FuzzText.swift
//  FuzzTest
//
//  Created by Nicholas Galasso on 8/29/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation
import CoreData


class FuzzText: FuzzObject {

    class func text(string:String)->FuzzText{
        let ctx = AppDelegate.sharedInstance().managedObjectContext
        let t = NSEntityDescription.insertNewObjectForEntityForName("FuzzText", inManagedObjectContext: ctx) as! FuzzText
        t.text = string
        return t
    }
}
